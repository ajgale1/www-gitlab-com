---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 3340 |
| [@vitallium](https://gitlab.com/vitallium) | 2 | 1700 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 3 | 1440 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 4 | 1370 |
| [@dblessing](https://gitlab.com/dblessing) | 5 | 1320 |
| [@tkuah](https://gitlab.com/tkuah) | 6 | 1240 |
| [@manojmj](https://gitlab.com/manojmj) | 7 | 1060 |
| [@sabrams](https://gitlab.com/sabrams) | 8 | 1030 |
| [@theoretick](https://gitlab.com/theoretick) | 9 | 900 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 10 | 900 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 11 | 900 |
| [@engwan](https://gitlab.com/engwan) | 12 | 840 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 13 | 830 |
| [@xanf](https://gitlab.com/xanf) | 14 | 740 |
| [@pks-t](https://gitlab.com/pks-t) | 15 | 700 |
| [@ifrenkel](https://gitlab.com/ifrenkel) | 16 | 700 |
| [@dsearles](https://gitlab.com/dsearles) | 17 | 700 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 18 | 700 |
| [@mrincon](https://gitlab.com/mrincon) | 19 | 640 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 20 | 620 |
| [@.luke](https://gitlab.com/.luke) | 21 | 620 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 22 | 610 |
| [@mksionek](https://gitlab.com/mksionek) | 23 | 600 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 24 | 600 |
| [@philipcunningham](https://gitlab.com/philipcunningham) | 25 | 600 |
| [@gonzoyumo](https://gitlab.com/gonzoyumo) | 26 | 600 |
| [@leipert](https://gitlab.com/leipert) | 27 | 580 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 28 | 580 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 29 | 500 |
| [@alexpooley](https://gitlab.com/alexpooley) | 30 | 500 |
| [@shreyasagarwal](https://gitlab.com/shreyasagarwal) | 31 | 500 |
| [@ash2k](https://gitlab.com/ash2k) | 32 | 500 |
| [@dsatcher](https://gitlab.com/dsatcher) | 33 | 500 |
| [@stanhu](https://gitlab.com/stanhu) | 34 | 480 |
| [@balasankarc](https://gitlab.com/balasankarc) | 35 | 470 |
| [@10io](https://gitlab.com/10io) | 36 | 470 |
| [@whaber](https://gitlab.com/whaber) | 37 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 38 | 400 |
| [@wortschi](https://gitlab.com/wortschi) | 39 | 400 |
| [@brodock](https://gitlab.com/brodock) | 40 | 400 |
| [@ggeorgiev_gitlab](https://gitlab.com/ggeorgiev_gitlab) | 41 | 400 |
| [@peterhegman](https://gitlab.com/peterhegman) | 42 | 400 |
| [@markrian](https://gitlab.com/markrian) | 43 | 360 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 44 | 340 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 45 | 320 |
| [@serenafang](https://gitlab.com/serenafang) | 46 | 320 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 47 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 48 | 300 |
| [@matteeyah](https://gitlab.com/matteeyah) | 49 | 300 |
| [@jameslopez](https://gitlab.com/jameslopez) | 50 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 51 | 300 |
| [@adamcohen](https://gitlab.com/adamcohen) | 52 | 300 |
| [@mwoolf](https://gitlab.com/mwoolf) | 53 | 250 |
| [@toupeira](https://gitlab.com/toupeira) | 54 | 230 |
| [@kerrizor](https://gitlab.com/kerrizor) | 55 | 220 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 56 | 220 |
| [@pshutsin](https://gitlab.com/pshutsin) | 57 | 220 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 58 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 59 | 200 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 60 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 61 | 200 |
| [@jcaigitlab](https://gitlab.com/jcaigitlab) | 62 | 200 |
| [@changzhengliu](https://gitlab.com/changzhengliu) | 63 | 200 |
| [@seanarnold](https://gitlab.com/seanarnold) | 64 | 180 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 65 | 180 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 66 | 170 |
| [@minac](https://gitlab.com/minac) | 67 | 170 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 68 | 150 |
| [@splattael](https://gitlab.com/splattael) | 69 | 140 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 70 | 140 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 71 | 140 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 72 | 140 |
| [@mkozono](https://gitlab.com/mkozono) | 73 | 140 |
| [@twk3](https://gitlab.com/twk3) | 74 | 130 |
| [@ekigbo](https://gitlab.com/ekigbo) | 75 | 130 |
| [@nfriend](https://gitlab.com/nfriend) | 76 | 120 |
| [@garyh](https://gitlab.com/garyh) | 77 | 120 |
| [@cngo](https://gitlab.com/cngo) | 78 | 110 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 79 | 110 |
| [@tomquirk](https://gitlab.com/tomquirk) | 80 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 81 | 100 |
| [@cablett](https://gitlab.com/cablett) | 82 | 100 |
| [@ahegyi](https://gitlab.com/ahegyi) | 83 | 90 |
| [@ebaque](https://gitlab.com/ebaque) | 84 | 90 |
| [@vsizov](https://gitlab.com/vsizov) | 85 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 86 | 80 |
| [@ck3g](https://gitlab.com/ck3g) | 87 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 88 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 89 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 90 | 80 |
| [@acroitor](https://gitlab.com/acroitor) | 91 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 92 | 80 |
| [@brytannia](https://gitlab.com/brytannia) | 93 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 94 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 95 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 96 | 60 |
| [@lulalala](https://gitlab.com/lulalala) | 97 | 60 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 98 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 99 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 100 | 60 |
| [@farias-gl](https://gitlab.com/farias-gl) | 101 | 60 |
| [@sming-gitlab](https://gitlab.com/sming-gitlab) | 102 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 103 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 104 | 50 |
| [@pslaughter](https://gitlab.com/pslaughter) | 105 | 40 |
| [@dgruzd](https://gitlab.com/dgruzd) | 106 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 107 | 40 |
| [@afontaine](https://gitlab.com/afontaine) | 108 | 40 |
| [@ghickey](https://gitlab.com/ghickey) | 109 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 110 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 111 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 112 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 113 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 114 | 30 |
| [@nmilojevic1](https://gitlab.com/nmilojevic1) | 115 | 30 |
| [@ohoral](https://gitlab.com/ohoral) | 116 | 30 |
| [@subashis](https://gitlab.com/subashis) | 117 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@greg](https://gitlab.com/greg) | 2 | 500 |
| [@nolith](https://gitlab.com/nolith) | 3 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 4 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 5 | 200 |
| [@mhuseinbasic](https://gitlab.com/mhuseinbasic) | 6 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 7 | 160 |
| [@smcgivern](https://gitlab.com/smcgivern) | 8 | 80 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 9 | 80 |
| [@rspeicher](https://gitlab.com/rspeicher) | 10 | 60 |
| [@aqualls](https://gitlab.com/aqualls) | 11 | 40 |
| [@rymai](https://gitlab.com/rymai) | 12 | 20 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 1000 |
| [@tywilliams](https://gitlab.com/tywilliams) | 2 | 300 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@imrishabh18](https://gitlab.com/imrishabh18) | 1 | 1500 |
| [@feistel](https://gitlab.com/feistel) | 2 | 1400 |
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 3 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 4 | 500 |
| [@behrmann](https://gitlab.com/behrmann) | 5 | 500 |
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 6 | 300 |
| [@stevemathieu](https://gitlab.com/stevemathieu) | 7 | 300 |
| [@abhijeet007rocks8](https://gitlab.com/abhijeet007rocks8) | 8 | 300 |
| [@cyberap](https://gitlab.com/cyberap) | 9 | 300 |
| [@law-lin](https://gitlab.com/law-lin) | 10 | 300 |
| [@Fall1ngStar](https://gitlab.com/Fall1ngStar) | 11 | 300 |
| [@wwwjon](https://gitlab.com/wwwjon) | 12 | 300 |
| [@tnir](https://gitlab.com/tnir) | 13 | 200 |

## FY22-Q4

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 1280 |
| [@vitallium](https://gitlab.com/vitallium) | 2 | 1100 |
| [@dblessing](https://gitlab.com/dblessing) | 3 | 1000 |
| [@ifrenkel](https://gitlab.com/ifrenkel) | 4 | 700 |
| [@dsearles](https://gitlab.com/dsearles) | 5 | 700 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 6 | 700 |
| [@gonzoyumo](https://gitlab.com/gonzoyumo) | 7 | 600 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 8 | 500 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 9 | 500 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 10 | 400 |
| [@ggeorgiev_gitlab](https://gitlab.com/ggeorgiev_gitlab) | 11 | 400 |
| [@peterhegman](https://gitlab.com/peterhegman) | 12 | 400 |
| [@matteeyah](https://gitlab.com/matteeyah) | 13 | 300 |
| [@jameslopez](https://gitlab.com/jameslopez) | 14 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 15 | 300 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 16 | 300 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 17 | 300 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 18 | 300 |
| [@mrincon](https://gitlab.com/mrincon) | 19 | 300 |
| [@adamcohen](https://gitlab.com/adamcohen) | 20 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 21 | 300 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 22 | 260 |
| [@jcaigitlab](https://gitlab.com/jcaigitlab) | 23 | 200 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 24 | 200 |
| [@theoretick](https://gitlab.com/theoretick) | 25 | 200 |
| [@pks-t](https://gitlab.com/pks-t) | 26 | 200 |
| [@changzhengliu](https://gitlab.com/changzhengliu) | 27 | 200 |
| [@garyh](https://gitlab.com/garyh) | 28 | 120 |
| [@xanf](https://gitlab.com/xanf) | 29 | 120 |
| [@minac](https://gitlab.com/minac) | 30 | 110 |
| [@ekigbo](https://gitlab.com/ekigbo) | 31 | 100 |
| [@manojmj](https://gitlab.com/manojmj) | 32 | 100 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 33 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 34 | 90 |
| [@pshutsin](https://gitlab.com/pshutsin) | 35 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 36 | 80 |
| [@splattael](https://gitlab.com/splattael) | 37 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 38 | 60 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 39 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 40 | 60 |
| [@balasankarc](https://gitlab.com/balasankarc) | 41 | 60 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 42 | 60 |
| [@engwan](https://gitlab.com/engwan) | 43 | 60 |
| [@mkozono](https://gitlab.com/mkozono) | 44 | 60 |
| [@sming-gitlab](https://gitlab.com/sming-gitlab) | 45 | 60 |
| [@toupeira](https://gitlab.com/toupeira) | 46 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 47 | 60 |
| [@mwoolf](https://gitlab.com/mwoolf) | 48 | 50 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 49 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 50 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 51 | 40 |
| [@10io](https://gitlab.com/10io) | 52 | 30 |
| [@ahegyi](https://gitlab.com/ahegyi) | 53 | 30 |
| [@subashis](https://gitlab.com/subashis) | 54 | 30 |
| [@mksionek](https://gitlab.com/mksionek) | 55 | 20 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 56 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@mhuseinbasic](https://gitlab.com/mhuseinbasic) | 2 | 200 |
| [@rspeicher](https://gitlab.com/rspeicher) | 3 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@tywilliams](https://gitlab.com/tywilliams) | 1 | 300 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@imrishabh18](https://gitlab.com/imrishabh18) | 1 | 1500 |
| [@abhijeet007rocks8](https://gitlab.com/abhijeet007rocks8) | 2 | 300 |
| [@cyberap](https://gitlab.com/cyberap) | 3 | 300 |
| [@law-lin](https://gitlab.com/law-lin) | 4 | 300 |
| [@Fall1ngStar](https://gitlab.com/Fall1ngStar) | 5 | 300 |
| [@wwwjon](https://gitlab.com/wwwjon) | 6 | 300 |

## FY22-Q3

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@djadmin](https://gitlab.com/djadmin) | 1 | 1660 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 2 | 1100 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 1000 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 730 |
| [@xanf](https://gitlab.com/xanf) | 5 | 620 |
| [@.luke](https://gitlab.com/.luke) | 6 | 620 |
| [@vitallium](https://gitlab.com/vitallium) | 7 | 600 |
| [@philipcunningham](https://gitlab.com/philipcunningham) | 8 | 600 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 580 |
| [@shreyasagarwal](https://gitlab.com/shreyasagarwal) | 10 | 500 |
| [@ash2k](https://gitlab.com/ash2k) | 11 | 500 |
| [@dsatcher](https://gitlab.com/dsatcher) | 12 | 500 |
| [@wortschi](https://gitlab.com/wortschi) | 13 | 400 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 14 | 400 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 15 | 400 |
| [@brodock](https://gitlab.com/brodock) | 16 | 400 |
| [@balasankarc](https://gitlab.com/balasankarc) | 17 | 300 |
| [@dblessing](https://gitlab.com/dblessing) | 18 | 240 |
| [@toupeira](https://gitlab.com/toupeira) | 19 | 170 |
| [@pshutsin](https://gitlab.com/pshutsin) | 20 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 21 | 120 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 22 | 100 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 23 | 90 |
| [@mwoolf](https://gitlab.com/mwoolf) | 24 | 90 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 25 | 90 |
| [@acroitor](https://gitlab.com/acroitor) | 26 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 27 | 80 |
| [@stanhu](https://gitlab.com/stanhu) | 28 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 29 | 80 |
| [@cngo](https://gitlab.com/cngo) | 30 | 80 |
| [@brytannia](https://gitlab.com/brytannia) | 31 | 80 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 32 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 33 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 34 | 60 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 35 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 36 | 60 |
| [@10io](https://gitlab.com/10io) | 37 | 60 |
| [@minac](https://gitlab.com/minac) | 38 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 39 | 60 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 40 | 60 |
| [@tomquirk](https://gitlab.com/tomquirk) | 41 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 42 | 60 |
| [@farias-gl](https://gitlab.com/farias-gl) | 43 | 60 |
| [@afontaine](https://gitlab.com/afontaine) | 44 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 45 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 46 | 40 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 47 | 30 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 48 | 30 |
| [@nmilojevic1](https://gitlab.com/nmilojevic1) | 49 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 50 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 51 | 30 |
| [@ohoral](https://gitlab.com/ohoral) | 52 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@smcgivern](https://gitlab.com/smcgivern) | 2 | 40 |
| [@rymai](https://gitlab.com/rymai) | 3 | 20 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@feistel](https://gitlab.com/feistel) | 1 | 1400 |
| [@behrmann](https://gitlab.com/behrmann) | 2 | 500 |
| [@stevemathieu](https://gitlab.com/stevemathieu) | 3 | 300 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@manojmj](https://gitlab.com/manojmj) | 1 | 900 |
| [@pks-t](https://gitlab.com/pks-t) | 2 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 4 | 400 |
| [@10io](https://gitlab.com/10io) | 5 | 380 |
| [@leipert](https://gitlab.com/leipert) | 6 | 380 |
| [@markrian](https://gitlab.com/markrian) | 7 | 360 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 8 | 340 |
| [@mksionek](https://gitlab.com/mksionek) | 9 | 320 |
| [@mrincon](https://gitlab.com/mrincon) | 10 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 11 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 12 | 300 |
| [@theoretick](https://gitlab.com/theoretick) | 13 | 300 |
| [@stanhu](https://gitlab.com/stanhu) | 14 | 300 |
| [@tkuah](https://gitlab.com/tkuah) | 15 | 240 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 16 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 120 |
| [@engwan](https://gitlab.com/engwan) | 18 | 100 |
| [@mkozono](https://gitlab.com/mkozono) | 19 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 20 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 21 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 22 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 23 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 24 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 25 | 70 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 26 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 27 | 60 |
| [@jerasmus](https://gitlab.com/jerasmus) | 28 | 60 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 29 | 60 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 30 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 31 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 32 | 60 |
| [@dgruzd](https://gitlab.com/dgruzd) | 33 | 40 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 34 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 35 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 36 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 37 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 38 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 39 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 40 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 41 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 42 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 43 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 600 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 1 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 2 | 500 |
| [@tnir](https://gitlab.com/tnir) | 3 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@leipert](https://gitlab.com/leipert) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 19 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@cablett](https://gitlab.com/cablett) | 24 | 100 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 25 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 26 | 80 |
| [@splattael](https://gitlab.com/splattael) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 29 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 32 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 33 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 34 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 38 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@cngo](https://gitlab.com/cngo) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


